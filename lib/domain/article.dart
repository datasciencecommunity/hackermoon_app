class Article {
  var title;
  var content;
  var category;
  var comments;

  Article({this.title, this.content, this.category, this.comments});
}
