import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:hackermoon_app/domain/category.dart';

abstract class CategoriesListState extends Equatable {
  const CategoriesListState();

  @override get props => [];
}

class DefaultCategoriesListState extends CategoriesListState {}
class ObtainedCategoriesList extends CategoriesListState {
  final results;

  const ObtainedCategoriesList(this.results);

  @override get props => [results];
}

abstract class CategoriesListEvent extends Equatable {
  const CategoriesListEvent();

  @override get props => [];
}

class GetCategoriesList extends CategoriesListEvent {}

class CategoriesListBloc extends Bloc<CategoriesListEvent, CategoriesListState> {

  @override get initialState => DefaultCategoriesListState();

  @override mapEventToState(CategoriesListEvent event) async* {
    if (event is GetCategoriesList) { yield ObtainedCategoriesList([
      Category(title: "TopSecret"),
      Category(title: "ForWife"),
      Category(title: "Tutorials"),
    ]); }
  }
}
