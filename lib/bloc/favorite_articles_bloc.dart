import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:hackermoon_app/domain/article.dart';

abstract class FavoriteArticlesState extends Equatable {
  const FavoriteArticlesState();

  @override get props => [];
}

class DefaultFavoriteArticlesState extends FavoriteArticlesState {}
class ObtainedFavoriteArticles extends FavoriteArticlesState {
  final results;

  const ObtainedFavoriteArticles(this.results);

  @override get props => [results];
}

abstract class FavoriteArticlesEvent extends Equatable {
  const FavoriteArticlesEvent();

  @override get props => [];
}

class GetFavoriteArticles extends FavoriteArticlesEvent {}

class FavoriteArticlesBloc extends Bloc<FavoriteArticlesEvent, FavoriteArticlesState> {
  @override get initialState => DefaultFavoriteArticlesState();

  @override mapEventToState(FavoriteArticlesEvent event) async* {
    if (event is GetFavoriteArticles) { yield ObtainedFavoriteArticles([
      Article(title: "Text1", content: "Content1"),
    ]); }
  }
}
