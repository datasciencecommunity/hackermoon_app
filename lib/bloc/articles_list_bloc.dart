import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:hackermoon_app/domain/article.dart';

abstract class ArticlesListState extends Equatable {
  const ArticlesListState();

  @override get props => [];
}

class DefaultArticlesListState extends ArticlesListState {}
class ObtainedArticlesList extends ArticlesListState {
  final results;

  const ObtainedArticlesList(this.results);

  @override get props => [results];
}

abstract class ArticlesListEvent extends Equatable {
  const ArticlesListEvent();

  @override get props => [];
}

class GetArticlesListForDay extends ArticlesListEvent {}

class ArticlesListBloc extends Bloc<ArticlesListEvent, ArticlesListState> {
  @override get initialState => DefaultArticlesListState();

  @override mapEventToState(ArticlesListEvent event) async* {
    if (event is GetArticlesListForDay) { yield ObtainedArticlesList([
      Article(title: "Text1", content: "Content1", category: "Tutorials", comments: ["hello", "hello", "hello", "hello", "hello", "hello", ]),
      Article(title: "Text2", content: "Content2", category: "Tutorials", comments: []),
      Article(title: "Text3", content: "Content3", category: "ForWife"),
    ]); }
  }
}
