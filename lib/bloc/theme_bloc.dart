import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class ThemeState extends Equatable {
  const ThemeState();

  @override get props => [];
}

class DefaultTheme extends ThemeState {
  final themeData = ThemeData(primarySwatch: Colors.green,);

  @override get props => [themeData];
}

abstract class ThemeEvent extends Equatable {
  const ThemeEvent();

  @override get props => [];
}

class SetDefaultTheme extends ThemeEvent {}

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  @override get initialState => DefaultTheme();

  @override mapEventToState(ThemeEvent event) async* {
    if (event is SetDefaultTheme) { yield DefaultTheme(); }
  }
}
