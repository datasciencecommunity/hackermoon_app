import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

abstract class NavigationState extends Equatable {
  const NavigationState();

  @override get props => [];
}

class ArticlesListNavState extends NavigationState {}
class FavoriteArticlesNavState extends NavigationState {}
class CategoriesNavState extends NavigationState {}

abstract class NavigationEvent extends Equatable {
  const NavigationEvent();

  @override get props => [];
}

class GoToArticlesList extends NavigationEvent {}
class GoToFavoriteArticles extends NavigationEvent {}
class GoToCategories extends NavigationEvent {}

class NavigationBloc extends Bloc<NavigationEvent, NavigationState> {
  @override get initialState => ArticlesListNavState();

  @override mapEventToState(NavigationEvent event) async* {
    if (event is GoToArticlesList) { yield ArticlesListNavState(); }
    else if (event is GoToFavoriteArticles) { yield FavoriteArticlesNavState(); }
    else if (event is GoToCategories) { yield CategoriesNavState(); }
  }
}
