import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hackermoon_app/bloc/categories_list_bloc.dart';

class ArticleViewPage extends StatefulWidget {

  final article;

  ArticleViewPage(this.article);

  @override createState() => _ArticleViewPage();
}

class _ArticleViewPage extends State<ArticleViewPage> {

  final comment = TextEditingController();

  @override build(context) => Scaffold(
    appBar: AppBar(title: Text(widget.article.title),),
    body: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(width: MediaQuery.of(context).size.width / 6, child: Container(
          color: Colors.black12,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _menuButtonAction(Icons.category, 'Category:\n${widget.article.category}', Icons.edit, () {
                _modalSetCategory();
              }),
              _menuButton(Icons.date_range, 'Deadline:\n2020-12-31'),
              _menuButton(Icons.done, 'Mark as read'),
              _menuButton(Icons.star, 'Add to favorites'),
              _menuButton(Icons.rate_review, 'Rate'),
              _menuButtonAction(Icons.comment, 'Comments', Icons.preview, () {
                _modalComments();
              }),
              _menuButton(Icons.share, 'Share'),
              _menuButton(Icons.delete, 'Delete'),
            ],
          ),
        ),),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _contentItem('Metadata: ...'),
            _contentItem(widget.article.content),
          ],
        ),
      ],
    ),
  );

  _menuButton(icon, text) => Padding(padding: EdgeInsets.only(left: 20, top: 20), child: Row(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Icon(icon, size: 24, color: Colors.black,),
      SizedBox(width: 10,),
      Text(text, style: TextStyle(fontSize: 18,),),
    ],
  ),);

  _menuButtonAction(icon, text, actionIcon, action) => Padding(padding: EdgeInsets.only(left: 20, right: 20, top: 20), child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(icon, size: 24, color: Colors.black,),
          SizedBox(width: 10,),
          Text(text, style: TextStyle(fontSize: 18,),),
        ],
      ),
      IconButton(icon: Icon(actionIcon, size: 24, color: Colors.black,), onPressed: action,)
    ],
  ),);

  _contentItem(text) => Padding(
    padding: EdgeInsets.only(left: 20, top: 20),
    child: Text(text, style: TextStyle(fontSize: 18,),),
  );

  _modalSetCategory() {
    BlocProvider.of<CategoriesListBloc>(context)..add(GetCategoriesList());
    showModalBottomSheet(
      context: context,
      builder: (context) => Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text("Choose category:", style: TextStyle(fontSize: 18,),),
            Row(children: [
              SizedBox(
                width: (MediaQuery.of(context).size.width - 40) / 20 * 19,
                child: BlocBuilder<CategoriesListBloc, CategoriesListState>(
                  builder: (context, state) {
                    var results = [];
                    if (state is ObtainedCategoriesList && state.results.length > 0) results = state.results;
                    return DropdownButtonFormField(
                      value: widget.article.category,
                      items: results.map<DropdownMenuItem>((cat) => DropdownMenuItem(
                        value: cat.title,
                        child: Text(cat.title),
                      ),).toList(),
                      onChanged: (value) {
                        // call change category api
                      },
                    );
                  },
                ),
              ),
              SizedBox(
                width: (MediaQuery.of(context).size.width - 40) / 20,
                child: IconButton(
                  icon: Icon(Icons.send),
                  onPressed: () {
                    var text = widget.article.category;
                    // call change category api
                  },
                ),
              )
            ],),
          ],
        ),
      ),
    );
  }

  _modalComments() {
    showModalBottomSheet(
      context: context,
      builder: (context) => Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text("Comments:", style: TextStyle(fontSize: 18,),),
            Expanded(child: ListView.builder(
              shrinkWrap: true,
              itemCount: widget.article.comments != null ? widget.article.comments.length : 0,
              itemBuilder: (context, i) => Padding(
                padding: EdgeInsets.all(20),
                child: Text(widget.article.comments[i], style: TextStyle(fontSize: 18)),
              ),
            ),),
            Row(children: [
              SizedBox(
                width: (MediaQuery.of(context).size.width - 40) / 20 * 19,
                child: TextField(
                  controller: comment,
                  decoration: InputDecoration(labelText: "Add comment:"),
                  onSubmitted: (value) => {
                    // call add comment api
                  },
                ),
              ),
              SizedBox(
                width: (MediaQuery.of(context).size.width - 40) / 20,
                child: IconButton(
                  icon: Icon(Icons.send),
                  onPressed: () {
                    var text = comment.text;
                    // call add comment api
                  },
                ),
              )
            ],),
          ],
        ),
      ),
    );
  }
}