import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hackermoon_app/bloc/articles_list_bloc.dart';
import 'package:hackermoon_app/bloc/categories_list_bloc.dart';
import 'package:hackermoon_app/domain/category.dart';
import 'package:hackermoon_app/pages/article_view.dart';

class ArticlesListPage extends StatefulWidget {

  @override createState() => _ArticlesListPage();
}

class _ArticlesListPage extends State<ArticlesListPage> {

  final articleName = TextEditingController();
  final articleContent = TextEditingController();
  var categoryValue = "";


  @override void didChangeDependencies() {
    super.didChangeDependencies();
    BlocProvider.of<ArticlesListBloc>(context).add(GetArticlesListForDay());
  }

  @override build(context) => Scaffold(
    body: BlocBuilder<ArticlesListBloc, ArticlesListState>(
      builder: (context, state) {
        if (state is ObtainedArticlesList && state.results.length > 0)
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: state.results.map<Widget>((entry) => GestureDetector(
              child: _item(entry.title),
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => ArticleViewPage(entry))),
            )).toList(),
          );
        return Column();
      }
    ),
    floatingActionButton: FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () => _modalArticle("Create"),
    ),
  );

  _item(text) => Padding(padding: EdgeInsets.only(left: 20, top: 20), child: Text(text, style: TextStyle(fontSize: 18,),),);

  _modalArticle(mode) {
    BlocProvider.of<CategoriesListBloc>(context)..add(GetCategoriesList());
    showModalBottomSheet(
      context: context,
      builder: (context) => Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text("$mode article", style: TextStyle(fontSize: 18,),),
            Row(children: [
              SizedBox(
                width: (MediaQuery.of(context).size.width - 40) / 20 * 19,
                child: TextField(
                  controller: articleName,
                  decoration: InputDecoration(labelText: "Name:"),
                  onSubmitted: (value) => {
                    // if (mode == "Create") call create api
                    // else if (mode == "Edit") call edit api
                  },
                ),
              )
            ],),
            Row(children: [
              SizedBox(
                width: (MediaQuery.of(context).size.width - 40) / 20 * 19,
                child: TextField(
                  controller: articleContent,
                  decoration: InputDecoration(labelText: "Content:"),
                  onSubmitted: (value) => {
                    // if (mode == "Create") call create api
                    // else if (mode == "Edit") call edit api
                  },
                ),
              ),
            ],),
            Row(children: [
              SizedBox(
                width: (MediaQuery.of(context).size.width - 40) / 20 * 19,
                child: BlocBuilder<CategoriesListBloc, CategoriesListState>(
                  builder: (context, state) {
                    var results = [];
                    if (state is ObtainedCategoriesList && state.results.length > 0) results = state.results;
                    results..add(Category(title: ""));
                    return DropdownButtonFormField(
                      value: categoryValue,
                      items: results.map<DropdownMenuItem>((cat) => DropdownMenuItem(
                        value: cat.title,
                        child: Text(cat.title),
                      ),).toList(),
                      onChanged: (value) {
                        // call change category api
                      },
                    );
                  }
              ),
              ),
            ],),
            Row(children: [
              Container(
                width: MediaQuery.of(context).size.width - 40,
                alignment: Alignment.bottomRight,
                child: IconButton(
                  icon: Icon(Icons.send),
                  onPressed: () {
                    var name = articleName.text;
                    var content = articleContent.text;
                    // if (mode == "Create") call create api
                    // else if (mode == "Edit") call edit api
                  },
                ),
              )
            ],),
          ],
        ),
      ),
    );
  }
}