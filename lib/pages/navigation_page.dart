import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hackermoon_app/bloc/navigation_bloc.dart';
import 'package:hackermoon_app/pages/articles_list.dart';
import 'package:hackermoon_app/pages/categories_list.dart';
import 'package:hackermoon_app/pages/favorite_articles.dart';

class NavigationPage extends StatefulWidget {

  @override createState() => _NavigationPage();
}

class _NavigationPage extends State<NavigationPage> {

  @override build(context) => Scaffold(
    appBar: AppBar(title: Text('HackerMoon'),),
    body: Row(
      children: [
        SizedBox(width: MediaQuery.of(context).size.width / 6, child: Container(
          color: Colors.black12,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _menuButton('Articles List', () => { BlocProvider.of<NavigationBloc>(context).add(GoToArticlesList()) }),
              _menuButton('Favorite Articles', () => { BlocProvider.of<NavigationBloc>(context).add(GoToFavoriteArticles()) }),
              _menuButton('Categories', () => { BlocProvider.of<NavigationBloc>(context).add(GoToCategories()) }),
            ],
          ),
        ),),
        SizedBox(width: MediaQuery.of(context).size.width / 6 * 5, child: BlocBuilder<NavigationBloc, NavigationState>(
            builder: (context, state) {
              if (state is ArticlesListNavState) return ArticlesListPage();
              else if (state is FavoriteArticlesNavState) return FavoriteArticlesPage();
              else if (state is CategoriesNavState) return CategoriesListPage();
              else return Text("Not supported");
            }
        ),),
      ],
    ),
  );

  _menuButton(text, action) => GestureDetector(
    child: Padding(padding: EdgeInsets.only(left: 20, top: 20), child: Text(text, style: TextStyle(fontSize: 18,),),),
    onTap: () => action(),
  );
}