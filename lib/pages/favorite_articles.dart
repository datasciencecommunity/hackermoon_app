import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hackermoon_app/bloc/favorite_articles_bloc.dart';
import 'package:hackermoon_app/pages/article_view.dart';

class FavoriteArticlesPage extends StatefulWidget {

  @override createState() => _FavoriteArticlesPage();
}

class _FavoriteArticlesPage extends State<FavoriteArticlesPage> {

  @override void didChangeDependencies() {
    super.didChangeDependencies();
    BlocProvider.of<FavoriteArticlesBloc>(context).add(GetFavoriteArticles());
  }

  @override build(context) => Scaffold(
    body: BlocBuilder<FavoriteArticlesBloc, FavoriteArticlesState>(
      builder: (context, state) {
        if (state is ObtainedFavoriteArticles && state.results.length > 0)
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: state.results.map<Widget>((entry) => GestureDetector(
              child: _item(entry.title),
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => ArticleViewPage(entry))),
            )).toList(),
          );
        return Column();
      }
    ),
  );

  _item(text) => Padding(padding: EdgeInsets.only(left: 20, top: 20), child: Text(text, style: TextStyle(fontSize: 18,),),);
}