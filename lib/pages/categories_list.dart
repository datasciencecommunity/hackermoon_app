import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hackermoon_app/bloc/categories_list_bloc.dart';

class CategoriesListPage extends StatefulWidget {

  @override createState() => _CategoriesListPage();
}

class _CategoriesListPage extends State<CategoriesListPage> {

  final categoryName = TextEditingController();

  @override void didChangeDependencies() {
    super.didChangeDependencies();
    BlocProvider.of<CategoriesListBloc>(context).add(GetCategoriesList());
  }

  @override build(context) => Scaffold(
    body: BlocBuilder<CategoriesListBloc, CategoriesListState>(
      builder: (context, state) {
        if (state is ObtainedCategoriesList && state.results.length > 0)
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: state.results.map<Widget>((entry) => GestureDetector(
              child: _item(entry.title),
            )).toList(),
          );
        return Column();
      }
    ),
    floatingActionButton: FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () => _modalCategory("Create"),
    ),
  );

  _item(text) => Padding(
    padding: EdgeInsets.only(left: 20, top: 20),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(text, style: TextStyle(fontSize: 18,),),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            IconButton(
              icon: Icon(Icons.edit, color: Colors.blue,),
              onPressed: () {
                categoryName.text = text;
                _modalCategory("Edit");
              },
            ),
            IconButton(
              icon: Icon(Icons.delete, color: Colors.red,),
              onPressed: () {
                // call api
              },
            )
          ],
        ),
      ],
    ),
  );

  _modalCategory(mode) => showModalBottomSheet(
    context: context,
    builder: (context) => Padding(
      padding: EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text("$mode category", style: TextStyle(fontSize: 18,),),
          Row(children: [
            SizedBox(
              width: (MediaQuery.of(context).size.width - 40) / 20 * 19,
              child: TextField(
                controller: categoryName,
                decoration: InputDecoration(labelText: "Name:"),
                onSubmitted: (value) => {
                  // if (mode == "Create") call create api
                  // else if (mode == "Edit") call edit api
                },
              ),
            ),
            SizedBox(
              width: (MediaQuery.of(context).size.width - 40) / 20,
              child: IconButton(
                icon: Icon(Icons.send),
                onPressed: () {
                  var text = categoryName.text;
                  // if (mode == "Create") call create api
                  // else if (mode == "Edit") call edit api
                },
              ),
            )
          ],),
        ],
      ),
    ),
  );
}