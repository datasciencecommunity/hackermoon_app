import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hackermoon_app/bloc/articles_list_bloc.dart';
import 'package:hackermoon_app/bloc/categories_list_bloc.dart';
import 'package:hackermoon_app/bloc/favorite_articles_bloc.dart';
import 'package:hackermoon_app/bloc/navigation_bloc.dart';
import 'package:hackermoon_app/bloc/theme_bloc.dart';
import 'package:hackermoon_app/pages/navigation_page.dart';

main() => runApp(MultiBlocProvider(
  providers: [
    BlocProvider<ThemeBloc>(create: (context) => ThemeBloc()..add(SetDefaultTheme()),),
    BlocProvider<ArticlesListBloc>(create: (context) => ArticlesListBloc(),),
    BlocProvider<FavoriteArticlesBloc>(create: (context) => FavoriteArticlesBloc(),),
    BlocProvider<CategoriesListBloc>(create: (context) => CategoriesListBloc(),),
    BlocProvider<NavigationBloc>(create: (context) => NavigationBloc(),),
  ],
  child: HackermoonApp(),
));

class HackermoonApp extends StatelessWidget {

  @override build(context) => BlocBuilder<ThemeBloc, ThemeState>(
    builder: (context, state) => MaterialApp(
      title: 'HackerMoon',
      theme: (state as DefaultTheme).themeData,
      home: NavigationPage(),
    ),
  );
}
